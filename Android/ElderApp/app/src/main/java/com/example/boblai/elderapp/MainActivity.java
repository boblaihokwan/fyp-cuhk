package com.example.boblai.elderapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    //View
    private FrameLayout content;

    // Fragment Stuff

    // Shared Var
    private boolean isBluetoothConnected = false;

    public static final int ADD_REMINDER = 210;
    public static final int MOD_REMINDER = 211;

    public interface ActResultPassThroughInterface{
        void onResult(Activity activity, int requestCode, int resultCode, Intent data);
    }
    ActResultPassThroughInterface actResultPassThroughInterface;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Log.d("onItemSelected", "method called");
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_bluetooth:
                    BTInfoFragment fragment =  BTInfoFragment.newInstance(isBluetoothConnected);
                    transaction.replace(R.id.content, fragment);
                    transaction.addToBackStack(null);
                    break;
                case R.id.navigation_contact:
                    ContactFragment contactFragment = new ContactFragment();
                    transaction.replace(R.id.content, contactFragment);
                    transaction.addToBackStack(null);
                    break;
                case R.id.navigation_reminder:
                    ReminderFragment reminderFragment = new ReminderFragment();
                    actResultPassThroughInterface = reminderFragment;
                    transaction.replace(R.id.content, reminderFragment);
                    transaction.addToBackStack(null);
                    break;
                default:
                    return false;
            }

            transaction.commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void onSettingAlarm(Intent data, boolean isNew){
        if(isNew){
            // insert new alarm
        }else{
            // change the alarm
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) return;

        Log.d("Main result", "Has result:" + requestCode + ", " + resultCode);

        switch(requestCode){
            case ADD_REMINDER:
            case MOD_REMINDER:
                actResultPassThroughInterface.onResult(this, requestCode, resultCode, data);
                break;
        }
    }
}
