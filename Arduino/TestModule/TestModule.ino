#include <SoftwareSerial.h>   // 引用程式庫
#include<Wire.h>
#include<math.h>
#include<string.h>
#include <I2Cdev.h>
#include <MPU6050.h>

#define ACX_OFFSET -4880
#define ACY_OFFSET -2634
#define ACZ_OFFSET 794
#define GYX_OFFSET 74
#define GYY_OFFSET -54
#define GYZ_OFFSET 5

MPU6050 accelgyro(0x68);
const int MPU_addr=0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
double result = 0;
//Sensor readings with offsets:  0 8 16390 0 0 0
//Your offsets: -4891 -2632 794 74  -54 5

// 定義連接藍牙模組的序列埠
SoftwareSerial BT(8, 9); // 接收腳, 傳送腳
char readBuffer[100];

void setup() {
  Serial.begin(9600);   // 與電腦序列埠連線
  Serial.println("BT is ready!");
  TWBR = 24;
  // 設定藍牙模組的連線速率
  // 如果是HC-05，請改成38400
  accelgyro.initialize();
  accelgyro.setXAccelOffset(ACX_OFFSET);
  accelgyro.setYAccelOffset(ACY_OFFSET);
  accelgyro.setZAccelOffset(ACZ_OFFSET);
  accelgyro.setXGyroOffset(GYX_OFFSET);
  accelgyro.setYGyroOffset(GYY_OFFSET);
  accelgyro.setZGyroOffset(GYZ_OFFSET);
  
  BT.begin(9600);
}

void sendIMUData(){
  accelgyro.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
  result = pow(pow(AcX, 2)+pow(AcY, 2)+pow(AcZ, 2), 0.5);
  
  BT.print("AcX: "); BT.println(AcX); 
  BT.print("AcY: "); BT.println(AcY); 
  BT.print("AcZ: "); BT.println(AcZ); 
  BT.print("GyX: "); BT.println(GyX); 
  BT.print("GyY: "); BT.println(GyY); 
  BT.print("GyZ: "); BT.println(GyZ); 
  BT.print("Result: "); BT.println(result);
}
 
void loop() {
    if (BT.available()) {
      char val = BT.read();
      if(val == '\n')
          sendIMUData();
    }
}
