import serial
import matplotlib.pyplot as plt
import json
import os.path
import struct
import pickle
from sklearn.preprocessing import scale
import numpy as np

devName = '/dev/tty.c079-DevB'
folderName = 'data'

#2 sec

def setupBT():
	ser = serial.Serial(devName, 57600,timeout = 0.5)
	# ser.open()
	return ser

def HextoString(inputStr):
	value = int(inputStr, 16)
	if(value > 0x7FFF):
		value |= ~(0x1111)
	return value

def readBT(ser):
	stateReadData = False
	dataStore = {}
	dataStore['AcX'] = []
	dataStore['AcY'] = []
	dataStore['AcZ'] = []
	dataStore['GyX'] = []
	dataStore['GyY'] = []
	dataStore['GyZ'] = []
	dataStore['time'] = []

	ser.write('ready')
	ser.flush()

	timecount = 0
	div = 10
	while True:
		if ser.inWaiting:
			inputStr = ser.readline()
			print inputStr
			if stateReadData == False and 'ready' in inputStr:
				print 'here'
				stateReadData = True
			elif stateReadData == True:
				if 'end' in inputStr:
					break

				print 'read data'
				dataHex = inputStr.replace(';', '').split(',')
				dataSet = map(HextoString, dataHex)
				print dataSet
				try:
					# dataAc = (((float(dataSet[0])**2 + float(dataSet[1])**2 + float(dataSet[2])**2)**0.5) / 8192)
					# dataGy = ((float(dataSet[3])**2 + float(dataSet[4])**2 + float(dataSet[5])**2)**0.5) / 65.5
	
					dataStore['AcX'].append(float(dataSet[0])/8192)
					dataStore['AcY'].append(float(dataSet[1])/8192)
					dataStore['AcZ'].append(float(dataSet[2])/8192)
					dataStore['GyX'].append(float(dataSet[3])/65.5)
					dataStore['GyY'].append(float(dataSet[4])/65.5)
					dataStore['GyZ'].append(float(dataSet[5])/65.5)

					# dataStore['Acc'].append(dataAc)
					# dataStore['Gy'].append(dataGy)

					dataStore['time'].append(timecount)
					timecount += div
				except:
					print inputStr


	ser.write('stop\n')
	ser.flush()		

	return dataStore

def storeDataInJson(dataStore, filename):
	count = 0
	finalName = filename + '_' + str(count) + '.json'
	path = os.path.join(folderName, finalName)
	while os.path.isfile(path):
		count += 1
		finalName = filename + '_' + str(count)  + '.json'
		path = os.path.join(folderName, finalName)
	f = open(path, 'w')
	f.write(json.dumps(dataStore))
	f.close()

def calTotal(x, y, z):
	return (x*x+y*y+z*z)**0.5

def plot(dataStore):
	AccTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, dataStore['AcX'], dataStore['AcY'], dataStore['AcZ'])
	GyTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, dataStore['GyX'], dataStore['GyY'], dataStore['GyZ'])

	fig, ax1 = plt.subplots()
	ax1.plot(dataStore['time'], AccTotal, 'r-')
	ax1.set_xlabel('time (s)')
	# Make the y-axis label, ticks and tick labels match the line color.
	ax1.set_ylabel('g', color='r')
	ax1.tick_params('y', colors='r')

	ax2 = ax1.twinx()
	# ax2.plot(dataStore['time'], dataStore['GyX'], 'c-', dataStore['time'], dataStore['GyY'], 'm-', dataStore['time'], dataStore['GyZ'], 'k-')
	ax2.plot(dataStore['time'], GyTotal, 'b-')
	ax2.set_ylabel('degree', color='b')
	ax2.tick_params('y', colors='b')

	fig.tight_layout()
	plt.show()

def svm_predict(dataStore):
	X = dataStore['AcX'] + dataStore['AcY'] + dataStore['AcZ'] + dataStore['GyX'] + dataStore['GyY'] + dataStore['GyZ']
	len(X)
	with open('svm.model', 'rb') as f:
		svm = pickle.load(f)
		print X
		X = scale(np.array([np.array(X)]), axis=1)
		print X[0]
		print X.shape
		# for x in X[0]:
		# 	print x
		print svm.predict(X)

if __name__ == '__main__':
	ser = setupBT()
	raw_input('press enter if ready')
	dataStore = readBT(ser)
	svm_predict(dataStore)
	filename = raw_input('please enter the filename:');
	storeDataInJson(dataStore, filename)
	# print dataStore
	ser.close()