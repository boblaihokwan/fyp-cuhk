#include <SoftwareSerial.h>   // 引用程式庫
#include<Wire.h>
#include<math.h>
#include<string.h>
#include <I2Cdev.h>
#include <MPU6050.h>

#define ACX_OFFSET -4880
#define ACY_OFFSET -2634
#define ACZ_OFFSET 794
#define GYX_OFFSET 74
#define GYY_OFFSET -54
#define GYZ_OFFSET 5

#define BT_READY_STR "ready"
#define BT_STOP_STR "stop"

#define RECORD_TIME 10000

MPU6050 accelgyro(0x68);
const int MPU_addr = 0x68; // I2C address of the MPU-6050
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
double result = 0;
//Sensor readings with offsets:  0 8 16390 0 0 0
//Your offsets: -4891 -2632 794 74  -54 5

//SoftwareSerial BT(8, 9); // 接收腳, 傳送腳

int sendIMUDataFlag = 0;
#define BTStatePin 7
unsigned long lastRecordTime = 0;
unsigned long commutativeTime = 0;

int lastBTConnectState = LOW;

void setup() {
  TWBR = 24;

  accelgyro.initialize();
  accelgyro.setXAccelOffset(ACX_OFFSET);
  accelgyro.setYAccelOffset(ACY_OFFSET);
  accelgyro.setZAccelOffset(ACZ_OFFSET);
  accelgyro.setXGyroOffset(GYX_OFFSET);
  accelgyro.setYGyroOffset(GYY_OFFSET);
  accelgyro.setZGyroOffset(GYZ_OFFSET);

  Serial.begin(57600);
  pinMode(BTStatePin, INPUT);
  //Serial.println("ready");
}

void sendIMUData() {
  accelgyro.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
  unsigned long currentTime = millis(); //get the time immediately
  if(lastRecordTime != 0 && currentTime > lastRecordTime){
    commutativeTime += currentTime - lastRecordTime;
  }
  lastRecordTime = currentTime;
  //CSV data string and send
  
//
//    DataBody* body = &dataArray[dataCount];
//    body->AcX = AcX;
//    body->AcY = AcY;
//    body->AcZ = AcZ;
//    body->GyX = GyX;
//    body->GyY = GyY;
//    body->GyZ = GyZ;
//    body->recordTime = commutativeTime;
//    dataCount++;
//
//    if(dataCount > 9){
//      int i;
//      for(i=0;i<100;i++){
//        DataBody* body = &dataArray[i];
//          BT.print(body->AcX); BT.print(",");
//          BT.print(body->AcY); BT.print(",");
//          BT.print(body->AcZ); BT.print(",");
//          BT.print(body->GyX); BT.print(",");
//          BT.print(body->GyY); BT.print(",");
//          BT.print(body->GyZ); BT.print(",");
//          BT.print(body->recordTime); BT.println(",");
//      }
//    }
  String timeStr = String(commutativeTime);
  //timeStr += "\n";
  //while(BT.overflow());
  
  //BT.print("[");
  Serial.flush();
  Serial.print(AcX); Serial.print(",");
  Serial.print(AcY); Serial.print(",");
  Serial.print(AcZ); Serial.print(",");
  Serial.print(GyX); Serial.print(",");
  Serial.print(GyY); Serial.print(",");
  Serial.print(GyZ); Serial.print(",");
  Serial.print(timeStr); Serial.println("");
  Serial.flush();

  
  
}

void loop() {
  
  
  if(sendIMUDataFlag == 1){
    sendIMUData();
    delay(50); 

    if(commutativeTime > RECORD_TIME){
      sendIMUDataFlag = 0;
      commutativeTime = 0;
      lastRecordTime = 0;
      Serial.println("end");
    }
  }

//  if(lastBTConnectState == LOW && digitalRead(BTStatePin) == HIGH){
//    BT.println("ready");
//    lastBTConnectState = HIGH;
//  }
  
  if(Serial.available()){
    String str = Serial.readString();
    if(str.equals(BT_READY_STR)){
      sendIMUDataFlag = 1;
      Serial.println("ready;");
    }else if(str.equals(BT_STOP_STR)){
      sendIMUDataFlag = 0;
      commutativeTime = 0;
      lastRecordTime = 0;
    }
  }
}
