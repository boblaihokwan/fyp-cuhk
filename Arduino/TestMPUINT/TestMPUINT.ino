#include <MsTimer2.h>

#include <SoftwareSerial.h>   // 引用程式庫
#include<Wire.h>
#include <I2Cdev.h>
#include <MPU6050.h>

MPU6050 mpu(0x68);

//#define ACX_OFFSET -4880
//#define ACY_OFFSET -2634
//#define ACZ_OFFSET 794
//#define GYX_OFFSET 74
//#define GYY_OFFSET -54
//#define GYZ_OFFSET 5


#define MPU_INT_PIN 2
#define LED_PIN 13
#define LSB_TO_G 9.8/8192.0
int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
bool isStartReadIMUData = false;
volatile bool isInterrputed = false;
volatile bool isTimer2Running = false;
volatile int count = 0;
volatile bool led = HIGH;

void setup() {
  // put your setup code here, to run once:
  TWBR = 24;
  mpu.initialize();
//  mpu.setXAccelOffset(ACX_OFFSET);
//  mpu.setYAccelOffset(ACY_OFFSET);
//  mpu.setZAccelOffset(ACZ_OFFSET);
//  mpu.setXGyroOffset(GYX_OFFSET);
//  mpu.setYGyroOffset(GYY_OFFSET);
//  mpu.setZGyroOffset(GYZ_OFFSET);
  mpu.setFullScaleAccelRange(1);
  mpu.setAccelerometerPowerOnDelay(3);

  // set free fall INT
//  mpu.setIntFreefallEnabled(true);
//  mpu.setFreefallDetectionThreshold(10);
//  mpu.setFreefallDetectionDuration(1);  
  mpu.setDHPFMode(1);
  
  mpu.setIntMotionEnabled(true);
  mpu.setMotionDetectionThreshold(2);
  mpu.setMotionDetectionDuration(40);

    
  mpu.setIntZeroMotionEnabled(true);
  mpu.setZeroMotionDetectionThreshold(2);
  mpu.setZeroMotionDetectionDuration(1);

  //attachInterrupt(digitalPinToInterrupt(MPU_INT_PIN), dmpDataReady, RISING);
  
  Serial.begin(9600);
  Serial.println("Started");
  pinMode(LED_PIN, OUTPUT);

  MsTimer2::set(10, readIMUData);
 //MsTimer2::start();
}

void dmpDataReady(){
  isInterrputed = true;
}

void readIMUData(){
  isStartReadIMUData = true;
}

void loop() {
  // put your main code here, to run repeatedly:
  if(isStartReadIMUData){
      mpu.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);  
      Serial.print(AcX * LSB_TO_G); Serial.print(",");
      Serial.print(AcY * LSB_TO_G); Serial.print(",");
      Serial.println(AcZ * LSB_TO_G); 
//      Serial.print(",");
//      Serial.print(GyX); Serial.print(",");
//      Serial.print(GyY); Serial.print(",");
//      Serial.print(GyZ); Serial.println(";");
      isStartReadIMUData = false;
  }

  if(mpu.getIntMotionStatus()){ 
      if(!isTimer2Running){
        isTimer2Running = true;
        MsTimer2::start();
      }
  }

  if(mpu.getIntZeroMotionStatus()){ 
    if(isTimer2Running){
      isTimer2Running = false;
      MsTimer2::stop();
    }
  }
}
