package com.example.boblai.elderapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bob Lai on 31/10/2017.
 */

public class AddRingtoneDialogFragment extends DialogFragment implements MediaRecorder.OnInfoListener,  MediaPlayer.OnCompletionListener {
    public static AddRingtoneDialogFragment newInstance(int title) {
        return new AddRingtoneDialogFragment();
    }

    TextInputEditText title;
    Button record, play, stop, posButton;
    MediaRecorder recorder;
    String tmpFile = "";
    boolean isRecording = false, isPlaying = false, isRecorded = false;
    MediaPlayer player;
    File file;
    AlertDialog dialog;

    public interface OnCompleteCallback{
        void onComplete(AddRingtoneDialogFragment fragment);
    }
    OnCompleteCallback callback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // set recorder
        recorder = new MediaRecorder();
        View view = View.inflate(getContext(), R.layout.dialog_reminder_record, null);
        title = (TextInputEditText) view.findViewById(R.id.etTitle);
        record = (Button) view.findViewById(R.id.ibRecord);
        play = (Button) view.findViewById(R.id.ibPlay);
        stop = (Button) view.findViewById(R.id.ibStop);
        stop.setEnabled(false);
        record.setEnabled(true);
        play.setEnabled(false);

        //set up file
        File file = new File(getActivity().getFilesDir() + "/record/");
        if(!file.exists()) file.mkdir();
        tmpFile = getActivity().getFilesDir() + "/record/tmp.3gp";

        // set listener
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    stopMediaPlayer();
                    setUpMediaRecorder();
                    recorder.prepare();
                    recorder.start();
                    v.setEnabled(false);
                    stop.setEnabled(true);
                    record.setEnabled(false);
                    play.setEnabled(false);
                    isRecording = true;
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(isRecording){
                recorder.stop();
                recorder.reset();
                isRecording = false;
                prepareMediaPlayer();
                isRecorded = true;
            }else if(isPlaying) {
                player.stop();
                isPlaying = false;
                play.setEnabled(true);
            }
            record.setEnabled(true);
            stop.setEnabled(false);

            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.start();
                isPlaying = true;
                stop.setEnabled(true);
                record.setEnabled(false);
                play.setEnabled(false);
            }
        });

        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("加入鈴聲");
        dialog.setView(view);
        dialog.setPositiveButton("確定", null);




        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                recorder.release();
                if (player != null)
                    player.release();
            }
        });

        this.dialog = dialog.create();



        return this.dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        posButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        posButton.setEnabled(false);
        posButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title.getText().toString().equals("")) {
                    showTitleError("Please enter the title.");
                }else{
                    recorder.release();
                    if (player != null)
                        player.release();
                    // move file
                    if(moveFile()){
                        dialog.dismiss();
                        callback.onComplete(AddRingtoneDialogFragment.this);
                        callback = null;
                    }
                }
            }
        });
    }

    // validation
    private void showTitleError(String msg){
        title.setError(msg);
        title.setFocusable(true);
        title.setFocusableInTouchMode(true);
        title.requestFocus();
    }

    // move file
    private boolean moveFile(){
        File chkFile = new File(getActivity().getFilesDir() + "/record/" + title.getText() + ".3gp");
        if(chkFile.exists()){
            showTitleError("This title has been used.");
            return false;
        }else{
            // move file
            File outputFile = new File(tmpFile);
            outputFile.renameTo(chkFile);
            return true;
        }
    }

    // media stuff
    private void setUpMediaRecorder(){
        try {
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            recorder.setAudioChannels(1);
            recorder.setMaxDuration(10000);
            recorder.setAudioSamplingRate(8000);
            recorder.setAudioEncodingBitRate(16);
            // FileOutputStream fos = getActivity().openFileOutput("tmp.3gp", Context.MODE_PRIVATE);
            recorder.setOnInfoListener(this);
            // recorder.setOutputFile(fos.getFD());
            recorder.setOutputFile(tmpFile);
        }catch (Exception e){ e.printStackTrace();}
    }

    private void prepareMediaPlayer(){
        try {
            player = new MediaPlayer();
            //FileInputStream fis = getContext().openFileInput("tmp.3gp");
            // player.setDataSource(fis.getFD());
            player.setDataSource(tmpFile);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnCompletionListener(this);
            player.prepare();
            play.setEnabled(true);
            posButton.setEnabled(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stopMediaPlayer(){
        if(isRecorded){
            player.release();
        }

    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        if(what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED){
            recorder.stop();
            recorder.reset();
            isRecording = false;
            prepareMediaPlayer();
            record.setEnabled(true);
            stop.setEnabled(false);
            isRecorded = true;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stop.setEnabled(false);
        play.setEnabled(true);
        record.setEnabled(true);
    }

    public void setOnCompleteCallback(OnCompleteCallback callback){
        this.callback = callback;
    }
}
