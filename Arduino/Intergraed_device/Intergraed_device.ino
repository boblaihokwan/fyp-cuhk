#include <SoftwareSerial.h>
#include "SIM808.h"
#include "IMU.h"
#include "datastore.h"

#define FALLING_DETECT "falling"
#define DATA_GET "dataget"
#define DATA_STORE "datastore"
#define PHONE "phone"
#define SOS "sos"

#define BN_PHONE1 7
#define BN_PHONE2 6
#define BN_PHONE3 5

int isPressing = 0;

void sendPhoneCall(int phoneNum){
  sendCommands(PHONE, String(phoneNum));
}

void decodeCommands(String cmd){
  int index = cmd.indexOf(";");
  String type = cmd.substring(0, index);
  String args = cmd.substring(index+1);
  
  type.trim();
  args.trim();
  
  if(type.equals(FALLING_DETECT)){
    commandIMU(args);
  }else if(type.equals(DATA_GET)){
    if(args.equals("all")){
       DataStore data;
      getData(&data);
      sendCommands(DATA_GET, String(data.duration) + "," +String(data.phone1) + "," +String(data.phone2) + "," +String(data.phone3) + "," +String(data.timestamp));
    }else if(args.equals("duration")){
      sendCommands(DATA_GET, String(getDuration()));
    }
   
  }else if(type.equals(DATA_STORE)){
    storeData(args);
  }else if(type.equals(PHONE)){
    
  }else if(type.equals(SOS)){
    if(args.equals("true")){
      setBuzzer(true);
      sendCommands(SOS, "Buzzer is set to true.");
    }else if(args.equals("false")){
      setBuzzer(false);
      sendCommands(SOS, "Buzzer is set to false.");
    }
  }
}

void sendCommands(String type, String args){
  Serial.println(type + ";" + args);
}

void setup() {
  // put your setup code here, to run once:
  setupSIM808();
  setupIMU();
  pinMode(BN_PHONE1, INPUT_PULLUP);
  pinMode(BN_PHONE2, INPUT_PULLUP);
  pinMode(BN_PHONE3, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(isPressing){
    if(digitalRead(isPressing) == HIGH){
      isPressing = 0;
    }
  }else{
    if(digitalRead(BN_PHONE1) == LOW){
      isPressing = BN_PHONE1;
      sendCommands(PHONE, String(1));
    }else if(digitalRead(BN_PHONE2) == LOW){
      isPressing = BN_PHONE2;
      sendCommands(PHONE, String(2));
    }else if(digitalRead(BN_PHONE3) == LOW){
      isPressing = BN_PHONE3;
      sendCommands(PHONE, String(3));
    }
  }
    

  //loopSIM808();
  loopIMU();
  if(Serial.available()){
    decodeCommands(Serial.readStringUntil('\n'));
  }
}
