#include <EEPROM.h>
#include "Arduino.h"
#include "datastore.h"

void strCopy(char* to, String from, int len){
  for(int i=0;i<len;i++){
    if(i == len-1) to[i] = '\0';
    else to[i] = from.charAt(i);
  }
}

void storeData(String args){
  DataStore data;
  int index = 0, lastindex = 0, count = 0;
  while(index != -1){
    index = args.indexOf(',', lastindex);
    if(count == -1){
      break;
    }

    if(count == 0){
      data.duration = args.substring(lastindex, index).toInt();
    }else if(count == 1){
      strCopy(data.phone1, args.substring(lastindex, index), 9);
    }else if(count == 2){
      strCopy(data.phone2, args.substring(lastindex, index), 9);
    }else if(count == 3){
      strCopy(data.phone3, args.substring(lastindex, index), 9);
    }else if(count == 4){
      data.timestamp = args.substring(lastindex, index).toInt();
    }
    //Serial.println(ret.substring(lastindex, index));
    lastindex = index + 1;
    count++;
  }
  EEPROM.put(0, data);
}

void getData(DataStore* data){
  EEPROM.get(0, *data);
}

int getDuration(){
  int duration;
  EEPROM.get(0, duration);
  return duration;
}

