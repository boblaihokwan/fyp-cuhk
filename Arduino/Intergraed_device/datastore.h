#ifndef DATASTORE_h
#define DATASTORE_h

typedef struct dataStore{
  int duration;
  char phone1[9], phone2[9], phone3[9];
  long timestamp;
} DataStore;

void storeData(String args);
void getData(DataStore* data);
int getDuration();

#endif
