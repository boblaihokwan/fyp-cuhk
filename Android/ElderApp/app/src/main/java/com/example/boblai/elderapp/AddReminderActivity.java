package com.example.boblai.elderapp;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class AddReminderActivity extends AppCompatActivity implements View.OnClickListener
        , AddRingtoneDialogFragment.OnCompleteCallback
        , MediaPlayer.OnCompletionListener, AdapterView.OnItemSelectedListener {
    // String[] ringtoneDummy = {"No ringtone", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    Spinner ringtone;
    ArrayAdapter<String> spinnerAdapter;
    ArrayList<String> spinnerList;
    EditText title;
    Button addRingtone, play, timeSub, timeAdd, ampmAdd, ampmSub;
    TextView time, ampm;

    final static int REQUEST_MICROPHONE = 122;
    final static int REQUEST_EXTERNAL_STORAGE = 689;

    String timeStr = "12:00";
    int timeNum = 12;
    boolean isAm = false;

    ReminderSqlHelper sqlHelper;

    MediaPlayer player;

    long dbID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);

        sqlHelper = new ReminderSqlHelper(this);

        ringtone = (Spinner) findViewById(R.id.spReminder);
        getFileList();
        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerList);
        ringtone.setAdapter(spinnerAdapter);
        ringtone.setOnItemSelectedListener(this);


        title = (EditText) findViewById(R.id.etTitle);


        time = (TextView) findViewById(R.id.tv_time);
        timeAdd = (Button) findViewById(R.id.bn_time_add);
        timeSub = (Button) findViewById(R.id.bn_time_subtract);
        timeAdd.setOnClickListener(this);
        timeSub.setOnClickListener(this);

        ampm = (TextView) findViewById(R.id.tv_ampm);
        ampmAdd = (Button) findViewById(R.id.bn_ampm_add);
        ampmSub = (Button) findViewById(R.id.bn_ampm_subtract);
        ampmAdd.setOnClickListener(this);
        ampmSub.setOnClickListener(this);

        addRingtone = (Button) findViewById(R.id.bnAddRingTone);
        addRingtone.setOnClickListener(this);

        play = (Button) findViewById(R.id.bnPlay);
        play.setOnClickListener(this);
        setTime();
        // if(getIntent().getExtras() != null) setViewWithIntentData();
    }

    private void setViewWithIntentData(){
        Bundle bundle = getIntent().getExtras();
        // TODO set the view
        dbID = bundle.getLong(ReminderSqlHelper.ReminderTableEntry._ID);
        title.setText(bundle.getString(ReminderSqlHelper.ReminderTableEntry.TITLE));
        boolean isRepeat = bundle.getInt(ReminderSqlHelper.ReminderTableEntry.REPEAT) == 1;
        Log.i("time", bundle.getString(ReminderSqlHelper.ReminderTableEntry.TIME));
        String timeStr = bundle.getString(ReminderSqlHelper.ReminderTableEntry.TIME);

        ringtone.setSelection(spinnerList.indexOf(bundle.getString(ReminderSqlHelper.ReminderTableEntry.RINGTONE)));
    }

    private void getFileList(){
        File recordFolder = new File(getFilesDir() + "/record/");
        String[] fileList = recordFolder.list();
        if(spinnerList == null)
            spinnerList = new ArrayList<>();
        else
            spinnerList.clear();
        spinnerList.add("無鈴聲");

        if(fileList != null){
            for(String file: fileList){
                spinnerList.add(file.replace(".3gp", ""));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v == addRingtone) {
            startRecordDialog();
        }

        if(v == play){
            if(player != null){
                player.start();
                play.setEnabled(false);
            }
        }

        if(v == timeAdd){
            timeNum++;
            if(timeNum == 12) isAm = !isAm;
            if(timeNum == 13) timeNum = 1;
        }else if(v == timeSub){
            timeNum--;
            if(timeNum == 0){
                timeNum = 12;
                isAm = !isAm;
            }
        }

        if(v == ampmAdd || v == ampmSub){
            isAm = !isAm;
        }
        setTime();
    }

    private void setTime(){
        time.setText(timeNum + ":00");
        ampm.setText(isAm ? "上午" : "下午");

        if(isAm){
            if(timeNum == 12){
                timeStr = "0:00";
            }else{
                timeStr = timeNum + ":00";
            }
        }else {
            if (timeNum != 12) {
                timeStr = (timeNum + 12) + ":00";
            } else {
                timeStr = timeNum + ":00";

            }
        }
    }

    private void startRecordDialog(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_MICROPHONE);

        }else if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
                AddRingtoneDialogFragment fragment = new AddRingtoneDialogFragment();
                fragment.setOnCompleteCallback(this);
                fragment.show(getSupportFragmentManager(), "ringtone");

        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if((requestCode == REQUEST_MICROPHONE || requestCode == REQUEST_EXTERNAL_STORAGE) && grantResults.length > 0){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startRecordDialog();
        }
    }

    @Override
    public void onComplete(AddRingtoneDialogFragment fragment) {
        getFileList();
        spinnerAdapter.notifyDataSetChanged();
    }

    private void prepareMediaPlayer(String filename){
        try {
            String filepath = getFilesDir() + "/record/" + filename + ".3gp";
            player = new MediaPlayer();
            player.setDataSource(filepath);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setOnCompletionListener(this);
            player.prepare();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_reminder_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_confirm){
            if(title.getText().toString().equals("")){
                // show error
            }else{
                // add to database
                ContentValues values = new ContentValues();
                values.put("title", title.getText().toString());
                if(ringtone.getSelectedItemPosition() == 0)
                    values.put("ringtone", "");
                else
                    values.put("ringtone", spinnerList.get(ringtone.getSelectedItemPosition()));

                values.put("time", timeStr);

                SQLiteDatabase db = sqlHelper.getWritableDatabase();
                if(dbID == -1){
                    long newId = db.insert(
                            "reminder",
                            null,
                            values
                    );
                }else{
                    db.update(
                            "reminder",
                            values,
                            "_id="+dbID,
                            null
                    );
                }


                Intent intent = new Intent();
                intent.putExtra("isNewData", true);


                if(getParent() == null){
                    setResult(Activity.RESULT_OK, intent);
                }else{
                    getParent().setResult(Activity.RESULT_OK, intent);
                }

                finish();
            }
        }

        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        play.setEnabled(true);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(player != null)
            player.release();
        if(i == 0){
            player = null;
        }else{
            prepareMediaPlayer(spinnerList.get(i));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
