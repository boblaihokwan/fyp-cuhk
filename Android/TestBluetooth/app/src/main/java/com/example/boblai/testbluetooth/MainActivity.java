package com.example.boblai.testbluetooth;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends FragmentActivity {
    private static final int REQUEST_ENABLE_BT = 72;
    private static final String TEMP_BLUETOOTH_NAME = "HC-05";
    BluetoothAdapter bluetoothAdapter;

    ArrayAdapter<String> arrayAdapter;
    //View
    ArrayList<String> listBTDevicesString;
    ArrayList<BluetoothDevice> listBTDevices;

    BluetoothService bluetoothService;
    boolean isFlusing = true;
    boolean isWaiting = true;

    GraphView graphView;
    BTDeviceDialogFragment btDeviceDialogFragment;

    TextView textView;

    LineGraphSeries<DataPoint> seriesAc, seriesGy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textview);


        //set up view
        setUpGraphView();
        //set up bluetooth
　       setUpBlueTooth();

    }

    private void setUpGraphView(){

        seriesAc = new LineGraphSeries<>();
        seriesGy = new LineGraphSeries<>();

        seriesAc.setColor(Color.BLUE);
        seriesGy.setColor(Color.RED);
        graphView = (GraphView) findViewById(R.id.graph);

        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setMaxX(10);
        graphView.getViewport().setMinX(0);
        graphView.addSeries(seriesAc);
        graphView.addSeries(seriesGy);
    }

    private class BTChoose implements BTDeviceDialogFragment.BluetoothDeviceChosenListner{

        @Override
        public void onChosen(int index) {
            BluetoothDevice device = listBTDevices.get(index);
            startBTCommunication(device);
            btDeviceDialogFragment.dismiss();
            unregisterReceiver(broadcastReceiver);
        }
    }

    private void startBTCommunication(BluetoothDevice device){

        bluetoothService = new BluetoothService(bluetoothAdapter, device, btHandler);
    }

    private final Handler btHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        switch(msg.what){
            case BluetoothService.MESSAGE_READ:
                String readMsg = (String) msg.obj;
                if(isWaiting){
                    if(readMsg.contains("ready")){
                        isWaiting = false;
                    }
                } else{
                    Log.i("handle data", readMsg);
                    handleData(readMsg);
                }


                break;
            case BluetoothService.MESSAGE_WRITE:
                break;
            case BluetoothService.BLUETOOTH_READY:
                bluetoothService.sendMessage("ready");
                Log.d("btHandler", "ready send");
                isWaiting = true;
                break;
        }
        }
    };

    private void handleData(String dataStr){
        Log.i("bt str", dataStr);
        double totalAcc = 0, totalGy = 0, timeCount = 0;
        try{
            String[] entries = dataStr.split(",");
            double AcX = Double.parseDouble(entries[0]);
            double AcY = Double.parseDouble(entries[1]);
            double AcZ = Double.parseDouble(entries[2]);
            double GyX = Double.parseDouble(entries[3]);
            double GyY = Double.parseDouble(entries[4]);
            double GyZ = Double.parseDouble(entries[5]);
            timeCount = Double.parseDouble(entries[6]) / 1000.0;

            totalAcc = Math.sqrt(Math.pow(AcX, 2)+Math.pow(AcY, 2)+Math.pow(AcZ, 2));
            totalGy = Math.sqrt(Math.pow(GyX, 2)+Math.pow(GyY, 2)+Math.pow(GyZ, 2));

            String str = dataStr + "\n"
                    + "totalAcc: " + totalAcc + "\ttotalGy: " + totalGy +"\n"
                    + "timecount: " + timeCount;
            Log.i("dataStr", str);
            textView.setText(str);

            seriesAc.appendData(new DataPoint(timeCount, totalAcc), true, 1000);
            seriesGy.appendData(new DataPoint(timeCount, totalGy), true, 1000);


        }catch(NumberFormatException e){
            e.printStackTrace();
            Log.d("data error", dataStr);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
            setUpGraphView();
            graphView.removeAllSeries();
            seriesAc.appendData(new DataPoint(timeCount, totalAcc), true, 1000);
            seriesGy.appendData(new DataPoint(timeCount, totalGy), true, 1000);
        }

    }

    private void flushingOldData(String inputStr){
        if(inputStr.contains("ready")){
            isFlusing = false;
            bluetoothService.sendMessage("ready");
            Log.i("BT communication", "ready");
        }
    }

    private void setUpBlueTooth(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        if(!bluetoothAdapter.isEnabled()){
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_ENABLE_BT);
        }else{

            listBTDevicesString = new ArrayList<>();
            listBTDevices = new ArrayList<>();
            arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listBTDevicesString);
            //lvBT.setAdapter(arrayAdapter);
            btDeviceDialogFragment = new BTDeviceDialogFragment();
            btDeviceDialogFragment.setArrayAdapter(arrayAdapter);
            btDeviceDialogFragment.setOnChosenListner(new BTChoose());
            btDeviceDialogFragment.show(getSupportFragmentManager(), "BT DEVICE");

            //find device
            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();
            if(pairedDevice.size() > 0){
                for(BluetoothDevice device : pairedDevice){
                    String deviceName = device.getName();
                    String deviceMac = device.getAddress();
                    //TODO check device list
                    listBTDevices.add(device);
                    listBTDevicesString.add(deviceName + ": " + deviceMac);
                }
            }
            //exploring device
            if(bluetoothAdapter.isDiscovering())
                bluetoothAdapter.cancelDiscovery();

            IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            registerReceiver(broadcastReceiver, intentFilter);
            bluetoothAdapter.startDiscovery();


        }
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("reciever action", "has actions");
            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                listBTDevices.add(device);
                Log.d("BT discover", deviceName + ": " + deviceHardwareAddress);
                //listBTDevices.add(deviceName + ": " + deviceHardwareAddress);
                arrayAdapter.add(deviceName + ": " + deviceHardwareAddress);
                //TODO check device list
                arrayAdapter.notifyDataSetChanged();
                //TODO device name check

                //TODO unregist receiver when found
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_ENABLE_BT){
            setUpBlueTooth();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(broadcastReceiver);
    }
}
