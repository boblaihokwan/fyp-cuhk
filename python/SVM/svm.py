from sklearn.model_selection import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import scale
from sklearn import svm
from os import listdir
from os.path import join
import json
import numpy as np
import matplotlib.pyplot as plt
import pickle

def load_data(mode=0):
	#  load adl data
	x_train, y_train, x_test, y_test, X, Y = [], [], [], [], [], []
	count_adl_div, count_fall_div = len(listdir('data/ADL'))*0.2, len(listdir('data/FALL'))*0.2
	print count_fall_div, count_adl_div
	for file, i in zip(listdir('data/ADL'), range(len(listdir('data/ADL')))):
		f = open(join('data/ADL', file), 'r')
		json_data = json.loads(f.read())
		if mode == 0 or mode == 2:
			X.append(json_data)
			Y.append(0)
		elif mode == 1:
			if i > count_adl_div:
				x_train.append(json_data)
				y_train.append(0)
			else:
				x_test.append(json_data)
				y_test.append(0)


	# load fall data
	for file, i in zip(listdir('data/FALL'), range(len(listdir('data/FALL')))):
		f = open(join('data/FALL', file), 'r')
		print file
		try:
			json_data = json.loads(f.read())
			if mode == 0 or mode == 2:
				X.append(json_data)
				Y.append(1)
			elif mode == 1:
				if i > count_fall_div:
					x_train.append(json_data)
					y_train.append(1)
				else:
					x_test.append(json_data)
					y_test.append(1)
		except:
			pass

	if mode == 0:
		x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)
	elif mode == 2:
		return X, Y

	return (x_train, y_train, x_test, y_test)

def process_data(data, y, mode=0):
	output = []
	new_y = []
	if mode == 0:
		for item in data:
			AccTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, item['AcX'], item['AcY'], item['AcZ'])
			GyTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, item['GyX'], item['GyY'], item['GyZ'])
			total = map(lambda x, y: (x << 16) | y, AccTotal, GyTotal)
			output.append(total)
	elif mode == 1:
		for item in data:
			output.append([np.array(item['AcX']), np.array(item['AcY']), np.array(item['AcZ']), np.array(item['GyX']), np.array(item['GyY']), np.array(item['GyZ'])])
	elif mode == 2:
		for item in data:
			AccTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, item['AcX'], item['AcY'], item['AcZ'])
			output.append(AccTotal)
	elif mode == 3:
		for item in data:
			GyTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, item['GyX'], item['GyY'], item['GyZ'])
			output.append(GyTotal)
	elif mode == 4:
		for item in data:
			tmp = item['AcX'] + item['AcY'] + item['AcZ'] + item['GyX'] + item['GyY'] + item['GyZ']	
			output.append(np.array(tmp))
	elif mode == 5:
		for item, target in zip(data, y):
			if target == 0:
				for i in range(0, 1001, 100):
					tmp = item['AcX'][i:i+500] + item['AcY'][i:i+500] + item['AcZ'][i:i+500] + item['GyX'][i:i+500] + item['GyY'][i:i+500] + item['GyZ'][i:i+500]
					if len(tmp)	== 3000:		
						output.append(np.array(tmp))
						new_y.append(0)
			else:
				tmp = item['AcX'][100:600] + item['AcY'][100:600] + item['AcZ'][100:600] + item['GyX'][100:600] + item['GyY'][100:600] + item['GyZ'][100:600]	
				# print len(tmp)
				output.append(np.array(tmp))
				new_y.append(1)
	else:
		return None

	return np.array(output), np.array(new_y)

def plot(data):
	fig, ax1 = plt.subplots()
	ax1.plot(range(0, 1001), data[0:1001], 'r-')
	ax1.set_xlabel('time (s)')
	plt.show()

def svm_model(x_train, y_train, x_test, y_test):
	# x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2)
	x_train = scale(x_train, axis=1)
	x_test = scale(x_test, axis=1)

	print x_train[0]
	svc = svm.SVC()
	svc.fit(x_train, y_train)
	print svc.score(x_test, y_test)

	pre = svc.predict(x_test)
	real = y_test
	print pre
	print real

def train_all_svm(x, y):
	# svc = GridSearchCV(cv=None, error_score='raise',
 #       estimator=svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
	#   decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
	#   max_iter=-1, probability=False, random_state=None, shrinking=True,
	#   tol=0.001, verbose=False),
 #       fit_params={}, iid=True, n_jobs=-1,
 #       param_grid=[{'kernel': ['linear'], 'C': [1, 10, 100, 1000]}, {'kernel': ['rbf'], 'gamma': [0.001, 0.0001], 'C': [1, 10, 100, 1000]}],
 #       pre_dispatch='2*n_jobs', refit=True, scoring=None, verbose=0)

	x = scale(x, axis=1)
	print x[0]
	svc = svm.SVC(C=1, kernel='rbf', gamma=0.001)
	svc.fit(x, y)
	print svc.score(x, y)
	# for file, i in zip(listdir('data/TEST'), range(len(listdir('data/FALL')))):
	# 	try:
	# 		f = open(join('data/TEST', file), 'r')
	# 		json_data = json.loads(f.read())
	# 		X = json_data['AcX'][100:600] + json_data['AcY'][100:600] + json_data['AcZ'][100:600] + json_data['GyX'][100:600] + json_data['GyY'][100:600] + json_data['GyZ'][100:600]
	# 		X = scale([X], axis=1)
	# 		print svc.predict(X), file
	# 	except:
	# 		pass
	save_model('svm.model', svc)

def save_model(filename, model):
	with open(filename, 'wb') as f:
		pickle.dump(model, f)

if __name__ == '__main__':
	# x_train, y_train, x_test, y_test = load_data()
	# x_train = process_data(x_train, 4)
	# x_test = process_data(x_test, 4)
	# print x_train[0]
	# svm_model(x_train, np.array(y_train), x_test, np.array(y_test))
	X, Y = load_data(mode=2)
	X, Y = process_data(X,Y, 5)
	train_all_svm(X, Y)

