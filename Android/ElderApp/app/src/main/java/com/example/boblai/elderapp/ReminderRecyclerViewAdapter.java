package com.example.boblai.elderapp;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Locale;

/**
 * Created by Bob Lai on 3/10/2017.
 */

public class ReminderRecyclerViewAdapter extends RecyclerView.Adapter<ReminderRecyclerViewAdapter.ReminderViewHolder> {
    String[] timeDummy = {"8:00 am", "9:00 am", "12:00 pm", "18:00 pm"};
    String[] titleDummy = {"eat food", "have medicine", "see doctor", "prepare food"};

    private Cursor data;
    private ReminderSqlHelper sqlHelper;
    private Context context;

    public static final int MOD_REMINDER = 211;

    public ReminderRecyclerViewAdapter(Context context){
        sqlHelper = new ReminderSqlHelper(context);
        this.context = context;
        retrieveData();
    }

    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_reminder, parent, false);
        return new ReminderViewHolder(view);
    }

    public void retrieveData(){

        SQLiteDatabase db = sqlHelper.getReadableDatabase();

        data = db.query("reminder",
                ReminderSqlHelper.ReminderTableEntry.getAllProjection(),
                null,
                null,
                null,
                null,
                null);
    }

    @Override
    public void onBindViewHolder(ReminderViewHolder holder, int position) {
        data.moveToPosition(position);
        Log.i("db", data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.TIME)));
        holder.id = data.getLong(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry._ID));
        String timeStr = data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.TIME));
        holder.time.setText(toFormatTime(timeStr));
        holder.title.setText(data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.TITLE)));
    }

    private String toFormatTime(String timeStr){
        String[] strSplited = timeStr.split(":");
        int hourOfDay = Integer.parseInt(strSplited[0]);
        int minute = Integer.parseInt(strSplited[1]);

        String output = "";
        if(hourOfDay >= 12) output += "下午";
        else output += "上午";
        if(hourOfDay == 0) output += 12;
        else if(hourOfDay > 12) output += hourOfDay - 12;
        else output += hourOfDay;

        output += ":" + String.format(Locale.ENGLISH, "%02d", minute) + " ";



        return output;
    }

    private String toFormatDate(int year, int month, int dayOfMonth){
        return String.format(Locale.ENGLISH, "%d-%d-%d", year, month+1, dayOfMonth);
    }

    @Override
    public int getItemCount() {
        return data.getCount();
    }

    public class ReminderViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
        TextView time, title;
        Switch enableSwitch;
        long id;
        int position;
        public ReminderViewHolder(View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.tvtime);
            title = (TextView) itemView.findViewById(R.id.tvtitle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            SQLiteDatabase db = sqlHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ReminderSqlHelper.ReminderTableEntry.ON, b);
            db.update("reminder", values, "_id=" + id, null);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            data.moveToPosition(position);
            Intent intent = new Intent(context, AddReminderActivity.class);

            // TODO put data in intent
            Bundle bundle = new Bundle();
            bundle.putString(ReminderSqlHelper.ReminderTableEntry.TITLE
                    , data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.TITLE)));
            bundle.putString(ReminderSqlHelper.ReminderTableEntry.DATE
                    , data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.DATE)));
            bundle.putString(ReminderSqlHelper.ReminderTableEntry.TIME
                    , data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.TIME)));
            bundle.putString(ReminderSqlHelper.ReminderTableEntry.LOCATION
                    , data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.LOCATION)));
            bundle.putString(ReminderSqlHelper.ReminderTableEntry.RINGTONE
                    , data.getString(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.RINGTONE)));
            bundle.putInt(ReminderSqlHelper.ReminderTableEntry.REPEAT
                    , data.getInt(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.REPEAT)));
            bundle.putInt(ReminderSqlHelper.ReminderTableEntry.VIBRATION
                    , data.getInt(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.VIBRATION)));
            bundle.putInt(ReminderSqlHelper.ReminderTableEntry.WEEKDAY
                    , data.getInt(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.WEEKDAY)));
            bundle.putLong(ReminderSqlHelper.ReminderTableEntry._ID
                    , data.getLong(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry._ID)));
            bundle.putInt(ReminderSqlHelper.ReminderTableEntry.ON
                    , data.getInt(data.getColumnIndex(ReminderSqlHelper.ReminderTableEntry.ON)));

            intent.putExtras(bundle);
            Activity act = (Activity) context;
            act.startActivityForResult(intent, MOD_REMINDER);
        }
    }
}
