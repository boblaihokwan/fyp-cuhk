#include <SoftwareSerial.h>
#include<math.h>
#include <I2Cdev.h>
#include <MPU6050.h>
#include <MsTimer2.h>

#define ACX_OFFSET -4880
#define ACY_OFFSET -2634
#define ACZ_OFFSET 794
#define GYX_OFFSET 74
#define GYY_OFFSET -54
#define GYZ_OFFSET 5

#define BT_READY_STR "ready"
#define BT_REALTIME_STR "realtime"
#define BT_STOP_STR "stop"
#define BT_END_STR "end"

#define RECORD_TIME 10000

MPU6050 accelgyro(0x68);
const int MPU_addr = 0x68; // I2C address of the MPU-6050

double result = 0;
//Sensor readings with offsets:  0 8 16390 0 0 0
//Your offsets: -4891 -2632 794 74  -54 5

//SoftwareSerial BT(8, 9); // 接收腳, 傳送腳

volatile int sendIMUDataFlag = 0;
int dataCount = 0;
int timeCount = 0;
#define DATA_COUNT_LIMIT 20
#define TIMER_TIME 10
#define TIME_COUNT_LIMIT 10000/TIMER_TIME
typedef struct dataBody{
  int16_t AcX, AcY, AcZ, GyX, GyY, GyZ;
} DataBody;
DataBody dataArray[DATA_COUNT_LIMIT];
int isOutputData = 0;
int isEnd = 0;
int mode = 0;

void setup() {
  TWBR = 24;

  accelgyro.initialize();
  accelgyro.setXAccelOffset(ACX_OFFSET);
  accelgyro.setYAccelOffset(ACY_OFFSET);
  accelgyro.setZAccelOffset(ACZ_OFFSET);
  accelgyro.setXGyroOffset(GYX_OFFSET);
  accelgyro.setYGyroOffset(GYY_OFFSET);
  accelgyro.setZGyroOffset(GYZ_OFFSET);

  accelgyro.setDLPFMode(2);
  accelgyro.setFullScaleAccelRange(1);
  accelgyro.setFullScaleGyroRange(1);
  
  MsTimer2::set(TIMER_TIME, sendIMUInt);
  Serial.begin(57600);
}

void outputData(){
  for(int i=0;i<dataCount;i++){
    Serial.print(dataArray[i].AcX); Serial.print(',');
    Serial.print(dataArray[i].AcY); Serial.print(',');
    Serial.print(dataArray[i].AcZ); Serial.print(',');
    Serial.print(dataArray[i].GyX); Serial.print(',');
    Serial.print(dataArray[i].GyY); Serial.print(',');
    Serial.print(dataArray[i].GyZ); Serial.println(',');
  }
  dataCount = 0;
  isOutputData = 0;
}

//void realTimeOutputHex(DataBody* body){
//  byte* byteBody = (byte *) body;
//  for(int i=0;i<sizeof(DataBody);i+=2){
//    Serial.write(byteBody[i]);
//    Serial.write(byteBody[i+1]);
//    Serial.print(',');
//  }
//  Serial.println(',');
//}
//

void toCharNumber(int16_t input){
  byte* byteBody =  (byte *) &input;
  while(Serial.availableForWrite() < 4);
  
  if(byteBody[1] < 0x10)
    Serial.print(0 , HEX);
  Serial.print(byteBody[1], HEX);
  
  while(Serial.availableForWrite() < 4);
  
  if(byteBody[0] < 0x10)
    Serial.print(0 , HEX);
  Serial.print(byteBody[0], HEX);
}
void realTimeOutputData2(int16_t AcX, int16_t AcY, int16_t AcZ, int16_t GyX, int16_t GyY, int16_t GyZ){
    toCharNumber(AcX);
    while(Serial.availableForWrite() < 2);
    Serial.print(',');
    toCharNumber(AcY);
    while(Serial.availableForWrite() < 2); 
    Serial.print(',');
    toCharNumber(AcZ);
    while(Serial.availableForWrite() < 2); 
    Serial.print(',');
    toCharNumber(GyX);
    while(Serial.availableForWrite() < 2); 
    Serial.print(',');
    toCharNumber(GyY);
    while(Serial.availableForWrite() < 2); 
    Serial.print(',');
    toCharNumber(GyZ);
    while(Serial.availableForWrite() < 2); 
    Serial.println();
}

void realTimeOutputData(int16_t AcX, int16_t AcY, int16_t AcZ, int16_t GyX, int16_t GyY, int16_t GyZ){
    String output = String(AcX) + ',' + String(AcY) + ',' + String(AcZ) + ',' + String(GyX) + ',' + String(GyY) + ',' + String(GyZ);
//    Serial.print(AcX); Serial.print(',');
//    Serial.print(AcY); Serial.print(',');
//    Serial.print(AcZ); Serial.print(',');
//    Serial.print(GyX); Serial.print(',');
//    Serial.print(GyY); Serial.print(',');
//    Serial.print(GyZ); Serial.println(',');
    while(Serial.availableForWrite() < output.length()*2+2); 
      Serial.println(output);
}

void sendIMUInt(){
  sendIMUDataFlag = 1;
  timeCount++;
  if(mode == 0 && timeCount > TIME_COUNT_LIMIT){
    MsTimer2::stop();
    isEnd = 1;
    timeCount = 0;
  }
}

void sendIMUData() {
  int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
  accelgyro.getMotion6(&AcX, &AcY, &AcZ, &GyX, &GyY, &GyZ);
//  DataBody body = {AcX, AcY, AcZ, GyX, GyY, GyZ};
  realTimeOutputData2(AcX, AcY, AcZ, GyX, GyY, GyZ);

  
//  dataArray[dataCount].AcX = AcX;
//  dataArray[dataCount].AcY = AcY;
//  dataArray[dataCount].AcZ = AcZ;
//  dataArray[dataCount].GyX = GyX;
//  dataArray[dataCount].GyY = GyY;
//  dataArray[dataCount].GyZ = GyZ;
//  
//  if(++dataCount >= DATA_COUNT_LIMIT) isOutputData = 1;
}

void loop() {
  
  
  if(sendIMUDataFlag == 1){
    sendIMUData();
    sendIMUDataFlag = 0;
    if(isEnd){
      Serial.println(BT_END_STR);
      isEnd = 0;
    }
  }

  if(isOutputData == 1){
    outputData();
  }

 
  if(Serial.available()){
    String str = Serial.readString();
    if(str.equals(BT_READY_STR)){
      MsTimer2::start();
      mode = 0;
      Serial.println("ready;");
    }else if(str.equals(BT_REALTIME_STR)){
      MsTimer2::start();
      mode = 1;
      Serial.println("ready;");
    }else if(str.equals(BT_STOP_STR)){
      MsTimer2::stop();
    }
  }
}
