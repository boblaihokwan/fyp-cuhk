package com.example.boblai.testbluetooth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Bob Lai on 15/9/2017.
 */

public class BTDeviceDialogFragment extends DialogFragment {
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;

    public interface BluetoothDeviceChosenListner{
        void onChosen(int index);
    }

    BluetoothDeviceChosenListner listner;
    //View
    public BTDeviceDialogFragment(){}

    public void setArrayAdapter(ArrayAdapter<String> arrayAdapter) {
        this.arrayAdapter = arrayAdapter;
    }

    public void setOnChosenListner(BluetoothDeviceChosenListner listner){
        this.listner = listner;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialogfragment_btdevice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = (ListView) view.findViewById(R.id.lvBTdevice);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listner.onChosen(position);
            }
        });
    }
}
