class KalmanFilter:
	def __init__(self, q, r):
		self.q = q
		self.r = r
		self.x = 0.0

		self.p = (q*q + r*r)**0.5



	def update(self, value):
		self.p += self.q
		k = self.p / (self.p + self.r)
		self.x += k * (value - self.x)
		self.p *= (1-k)
		return self.x