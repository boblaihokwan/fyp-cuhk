#ifndef SIM808_h
#define SIM808_h

void setupSIM808();
void loopSIM808();
void setBuzzer(int isSound);

#endif
