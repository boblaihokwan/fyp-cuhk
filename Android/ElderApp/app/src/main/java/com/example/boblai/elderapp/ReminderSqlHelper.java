package com.example.boblai.elderapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bob Lai on 6/11/2017.
 */

public class ReminderSqlHelper extends SQLiteOpenHelper{
    public ReminderSqlHelper(Context context) {
        super(context, "elder.db", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL( "CREATE TABLE reminder " +
                    "(_id INTEGER PRIMARY KEY NOT NULL, " +
                    "date DATETIME, " +
                    "time DATETIME, " +
                    "title VARCHAR," +
                    "repeat BOOLEAN,"+
                    "weekday INTERGER," +
                    "location VARCHAR," +
                    "ringtone VARCHAR," +
                    "isOn BOOLEAN,"+
                    "vibration BOOLEAN)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static class ReminderTableEntry{
        public ReminderTableEntry() {}

        final static String _ID = "_id";
        final static String DATE = "date";
        final static String TIME = "time";
        final static String TITLE = "title";
        final static String REPEAT = "repeat";
        final static String WEEKDAY = "weekday";
        final static String LOCATION = "location";
        final static String RINGTONE = "ringtone";
        final static String VIBRATION = "vibration";
        final static String ON = "isOn";

        public static String[] getAllProjection(){
            String[] projection = {_ID, DATE, TIME, TITLE, REPEAT, WEEKDAY, LOCATION, RINGTONE, VIBRATION, ON};
            return projection;
        }
    }
}
