import pickle
from sklearn_porter import Porter

f = open('svm.model', 'rb')
clf = pickle.load(f)
f.close()

# Export:
porter = Porter(clf, language='java')
output = porter.export(embedded=False)

with open('svm.java', 'w') as o:
	o.write(output)
# print(output)