import os.path
import sys
import json
import matplotlib.pyplot as plt
from scipy.signal import resample
folderName = 'data'

def plot(dataStore, mode='total'):
	if(mode == 'total'):
		AccTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, dataStore['AcX'], dataStore['AcY'], dataStore['AcZ'])
		GyTotal = map(lambda x, y, z: (x*x+y*y+z*z)**0.5, dataStore['GyX'], dataStore['GyY'], dataStore['GyZ'])

	fig, ax1 = plt.subplots()
	ax1.plot(range(len(dataStore['AcX'])), AccTotal, 'r-')
	ax1.set_xlabel('time (s)')
	# Make the y-axis label, ticks and tick labels match the line color.
	ax1.set_ylabel('g', color='r')
	ax1.tick_params('y', colors='r')

	ax2 = ax1.twinx()
	ax2.plot(range(len(dataStore['GyX'])), GyTotal, 'b-')
	ax2.set_ylabel('degree', color='b')
	ax2.tick_params('y', colors='b')

	fig.tight_layout()
	plt.show()

def getData(filename):
	if not filename.endswith('.json'):
		filename += '.json'

	path = os.path.join(folderName, filename)
	f = open(path, 'r')
	return json.loads(f.read())

if __name__ == '__main__':
	dataStore = getData(sys.argv[1])
	# for key in dataStore.keys():
	# 	dataStore[key] = resample(dataStore[key], 500)
	plot(dataStore)