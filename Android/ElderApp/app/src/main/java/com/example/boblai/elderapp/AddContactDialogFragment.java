package com.example.boblai.elderapp;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Bob Lai on 5/2/2018.
 */

public class AddContactDialogFragment extends android.support.v4.app.DialogFragment {
    AlertDialog dialog;
    int numOfContact = 1;
    public static AddContactDialogFragment newInstance(int numOfContact) {
        AddContactDialogFragment fragment =  new AddContactDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("numOfContact", numOfContact);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            numOfContact = getArguments().getInt("numOfContact");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_add_contact, null);
        // set view
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("聯絡人"  + numOfContact + ":");
        dialog.setView(view);

        dialog.setPositiveButton("Confirm", null);
        dialog.setNegativeButton("Cancel", null);
        this.dialog = dialog.create();
        return this.dialog;
    }
}
