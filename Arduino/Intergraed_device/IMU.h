#ifndef IMU_h
#define IMU_h

void setupIMU();
void loopIMU();
void commandIMU(String args);

#endif
