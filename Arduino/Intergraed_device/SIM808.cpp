#include <SoftwareSerial.h>
SoftwareSerial SIM808(8, 9);
#include "Arduino.h"
#include "datastore.h"

#define SOS_BUTTON 3
#define BUZZER 4
#define CANCEL_BUTTON 2

#define COUNTING_TIME 3000
#define CHECKING_TIME 3000

#define RECEIVE_COMMAND "get location"

int isCounting = 0;
long timeCountStart = 0;
long lastSMScheckTime = 0;
int isSAPBRSetted = 0;

int countingtime = 0;

int isCheckingSMS = 0;

String sendATCommandWithRet(char* command, char* expacted){
  SIM808.print(command);
  while(1){
    String ret = SIM808.readStringUntil('\r');
    ret.trim();
    if(ret.startsWith(expacted)){
      ret.replace(expacted, "");
      return ret;
    }
  }
}

int sendATCommandCharWithCheck(char command){
  SIM808.write(command);
  int count = 0;
  while(1){
    String ret = SIM808.readStringUntil('\r');
    ret.trim();
    if(ret.equals("OK")){
      return 1;
    }else if(ret.equals("ERROR")){
      return 0;
    }
  }
}

int sendATCommandWithCheck(char* command){
  SIM808.print(command);
  int count = 0;
  while(1){
    String ret = SIM808.readStringUntil('\r');
    ret.trim();
    if(ret.equals("OK")){
      return 1;
    }else if(ret.equals("ERROR")){
      return 0;
    }
  }
}

int sendATCommandWithRetUntilOK(char* command,char* expacted, char* compare){
  SIM808.print(command);
  int count = 0;
  int isSame = 0;
  int nextIsMsg = 0;
  while(1){
    String ret = SIM808.readStringUntil('\r');
    ret.trim();
    if(nextIsMsg){
      if(ret.equals(compare)){
        isSame = 1;
      }
      nextIsMsg = 0;
    }
    if(ret.startsWith(expacted)){
      ret.replace(expacted, "");
      nextIsMsg = 1;
    }
    
    if(ret.equals("OK")){
      return isSame;
    }else if(ret.equals("ERROR")){
      return 0;
    }
  }
}

void getLocation(char* lat, char* lon){
  int status = 0, lastindex = 0, index=0, count = 0;
  String ret;
  // Serial.println("getting location");
  if(isSAPBRSetted == 0){
    status = sendATCommandWithCheck("AT+SAPBR=1,1\r");
    sendATCommandWithRet("AT+CIPGSMLOC=1,1\r", "+CIPGSMLOC: ");
  }else{
    status = 1;
  }
    
  
  if(1){
    isSAPBRSetted = 1;
    // Serial.print("status1:OK");
    ret = sendATCommandWithRet("AT+CIPGSMLOC=1,1\r", "+CIPGSMLOC: ");
    // Serial.println("getStr: " + ret);
    while(index != -1){
      index = ret.indexOf(',', lastindex);
      if(count == -1){
        break;
      }

      if(count == 1){
        //Serial.println("count 2");
        ret.substring(lastindex, index).toCharArray(lon, 20);
      }else if(count == 2){
        //Serial.println("count 3");
        ret.substring(lastindex, index).toCharArray(lat, 20);
      }
      //Serial.println(ret.substring(lastindex, index));
      lastindex = index + 1;
      count++;
    }
    
    //Serial.println("status2:" + ret);
  }else{
    //Serial.print("status1:ERROR");
  }
}

void sendLOCSMS(int isEmergency){
  char lat[20];
  char lon[20];
  int status = 0;
  getLocation(lat, lon);

  //send SMS
  //Serial.println("send SMS");
  sendATCommandWithCheck("AT+CSCA=\"+85290288000\"\r");
  sendATCommandWithCheck("AT+CMGF=1\r");
  sendATCommandWithRet("AT+CMGS=\"+85251285651\"\r", ">");
  SIM808.print("https://www.google.com/maps/place/"); SIM808.print(lat); SIM808.print(","); SIM808.print(lon);
  SIM808.print("/@"); SIM808.print(lat); SIM808.print(","); SIM808.print(lon); SIM808.print(",18z\r");
  status = sendATCommandCharWithCheck(0x1A);
  //Serial.print("send SMS success:"); Serial.println(status);
}

void checkSMS(){
  int status = sendATCommandWithRetUntilOK("AT+CMGL=\"REC UNREAD\"\r", "+CMGL: ", RECEIVE_COMMAND);
  if(status){
    //Serial.println("Command res, send SMS");
    sendLOCSMS(false);
  }else{
    //Serial.println("No match com");
  }
}
void setupSIM808() {
  // put your setup code here, to run once:
  pinMode(SOS_BUTTON, INPUT_PULLUP);
  pinMode(CANCEL_BUTTON, INPUT_PULLUP);
  pinMode(BUZZER, OUTPUT);
  digitalWrite(BUZZER, LOW);
  
  SIM808.begin(9600);
  //Serial.begin(57600);
  //Serial.println("Serial is Ready");
}

void setBuzzer(int isSound){
  digitalWrite(BUZZER, isSound);
}

//int getDuration(){
//  DataStore data;
//  getData(data);
//  return data.duration;
//}

void loopSIM808() {
  // Handle SOS function
  if(isCounting == 0 && digitalRead(SOS_BUTTON) == LOW){
     isCounting = 1;
     timeCountStart = millis();
     countingtime = getDuration();
     digitalWrite(BUZZER, HIGH);
  }
  
  if(isCounting == 1 && digitalRead(CANCEL_BUTTON) == LOW){
    isCounting = 0;
    digitalWrite(BUZZER, LOW);
  }

  if(isCounting == 1 && ((millis() - timeCountStart) > countingtime)){
     isCounting = 0;
     isCheckingSMS = 0;
     digitalWrite(BUZZER, LOW);
     sendLOCSMS(true);
  }
  
  if(isCheckingSMS){
//    Serial.print("chk sms ret");
//    Serial.println(SIM808.available());
     if(SIM808.available()){
       String ret = SIM808.readStringUntil('\r');
       //Serial.println(ret);
        ret.trim();
        if(ret.startsWith("+CMGL: ")){
          String ret = SIM808.readStringUntil('\r');
          //Serial.println(ret);
          ret.trim();
          if(ret.equals(RECEIVE_COMMAND)){
            sendLOCSMS(false);
            isCheckingSMS = 0;
          }
        }else if(ret.equals("OK")){
            isCheckingSMS = 0;
        }else if(ret.equals("ERROR")){
          isCheckingSMS = 0;
        } 
     }
  }else if(isCounting == 0 && millis() - lastSMScheckTime > CHECKING_TIME){
     //Serial.println("check sms");
      SIM808.print("AT+CMGL=\"REC UNREAD\"\r");
      isCheckingSMS = 1;
     //checkSMS();
     lastSMScheckTime = millis();
   }
}
