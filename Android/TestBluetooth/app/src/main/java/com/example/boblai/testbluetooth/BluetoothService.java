package com.example.boblai.testbluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

import static android.util.Log.i;

/**
 * Created by Bob Lai on 15/9/2017.
 */

public class BluetoothService {
    private BluetoothAdapter bluetoothAdapter;
    private Handler BTHandler;

    private ConnectThread connectThread;
    private CommunicationThread communicationThread;

    public static final int MESSAGE_READ = 0;
    public static final int MESSAGE_WRITE = 1;
    public static final int BLUETOOTH_READY = 2;

    public BluetoothService(BluetoothAdapter bluetoothAdapter,BluetoothDevice device, Handler handler){
        this.bluetoothAdapter = bluetoothAdapter;
        this.BTHandler = handler;

        connectThread = new ConnectThread(device);
        connectThread.start();
    }

    private class ConnectThread extends Thread{
        private final BluetoothSocket socket;
        private final BluetoothDevice device;


        private final UUID mUUID;

        public ConnectThread(BluetoothDevice device){
            BluetoothSocket tmp = null;
            this.device = device;
            mUUID = UUID.fromString("b9eb58c8-8e64-4460-b47f-c7a2fef1c802");

            try{
                //tmp = device.createRfcommSocketToServiceRecord(mUUID);
                //tmp = device.createInsecureRfcommSocketToServiceRecord(mUUID);
//                tmp = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);

                Class<?> clazz = device.getClass();
                Class<?>[] paramTypes = new Class<?>[] {Integer.TYPE};

                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[] {Integer.valueOf(1)};

                tmp = (BluetoothSocket) m.invoke(device, params);
                //fallbackSocket.connect();

            }catch (Exception e){
                e.printStackTrace();
            }

            socket = tmp;
        }

        public void run(){
            Log.d("connect T", "T started");
            bluetoothAdapter.cancelDiscovery();

            try{
                socket.connect();
                Log.d("connect T", "socket connected");
            }catch (IOException connectException){
                connectException.printStackTrace();
                try {
                    socket.close();
                }catch (IOException e){
                    e.printStackTrace();
                }

                return;
            }

            //manage socket
            connectedBTAction(socket);
        }

        public void cancel(){
            try {
                socket.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }


    private synchronized void connectedBTAction(BluetoothSocket socket){
        //
        communicationThread = new CommunicationThread(socket);
        communicationThread.start();
        BTHandler.obtainMessage(BLUETOOTH_READY).sendToTarget();

        Log.d("BT service", "Communication started");
    }

    public void sendMessage(String msg){
        byte[] byteData = msg.getBytes();
        communicationThread.write(byteData);
    }

    private class CommunicationThread extends Thread{
        private final BluetoothSocket socket;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        private byte[] buffer;
        private final String DELIMITER = ";";

        public CommunicationThread(BluetoothSocket socket){
            this.socket = socket;
            InputStream tempInput = null;
            OutputStream tempOuptut = null;
            try{
                tempInput = socket.getInputStream();
            }catch(IOException e){
                e.printStackTrace();
            }

            try{
                tempOuptut = socket.getOutputStream();
            }catch(IOException e){
                e.printStackTrace();
            }

            this.inputStream = tempInput;
            this.outputStream = tempOuptut;
        }

        public void run(){
            buffer = new byte[1024];
            String buf = "";
            int byteNum;
            boolean hasData = false;

            int bytes = 0;
            int begin = 0;

            while(true){
                try{
                    //Log.i("av", "" + inputStream.available());
                    if(inputStream.available() > 0) {
                        bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                        i("BT com", "read, begin: " + begin + " byte: " + bytes);
                        for(int i = begin; i < bytes; i++) {
                            if(buffer[i] == ";".getBytes()[0]) {
                                String strToSend = new String(buffer);
                                i("BT com", strToSend);
                                strToSend = strToSend.substring(begin, i);
                                BTHandler.obtainMessage(MESSAGE_READ, strToSend.length(), i, strToSend).sendToTarget();
                                begin = i + 1;
                            }
                        }


                        if(bytes >= 1024){
                            int count = 0;
                            for(int i=begin; i < bytes; i++){
                                buffer[count++] = buffer[i];
                            }
                            begin = 0;
                            bytes = count;
                        }


//                        byteNum = inputStream.read(buffer, 0, 1024);
//
//                        String str = new String(buffer);
////                       Log.i("BT communication",  "byte num: " + byteNum + ", get MSG: " + str);
////                        Log.i("BT communication", "index:" + str.lastIndexOf(DELIMITER) + ", len:" + str.length());
//                        String realStr = str.substring(0, byteNum);
//                        //Log.i("BT communication", "str:" + realStr + ", num: " + byteNum);
//                        buf += realStr;
//                        Log.i("BT communication", "buf:" + buf);
//                        String[] dataArray = buf.split("\n");
//                        for(int i=0;i<dataArray.length;i++){
//                            String dataStr = dataArray[i].replaceAll("\\s+","");
//                            Log.i("BT communication", "datastr:" + dataStr);
//                            if(i == dataArray.length-1 && !dataStr.endsWith(DELIMITER) && !dataStr.contains("ready")){
//                                buf = dataStr;
//                                break;
//                            }
//
//                            if(dataStr.length() > 0){
//                                Message readMsg = BTHandler.obtainMessage(MESSAGE_READ, dataStr.length(), -1, dataStr);
//                                readMsg.sendToTarget();
//                                hasData = true;
//                            }
//                            if(i == dataArray.length-1) buf = "";
//                        }
//
//                        //Log.i("BT communication", "after buf:" + buf);
                    }


                    //SystemClock.sleep(100);
                }catch (IOException e){
                    e.printStackTrace();
                    break;
                }catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        public void write(byte[] output){
            try{
                outputStream.write(output);
                Message writeMsg = BTHandler.obtainMessage(MESSAGE_WRITE, -1, -1, buffer);
                writeMsg.sendToTarget();

            } catch (IOException e){
                e.printStackTrace();
            }
        }

        public void cancel(){
            try {
                socket.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
