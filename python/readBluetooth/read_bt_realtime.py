import serial
import matplotlib.pyplot as plt
import json
import os.path
import struct
import pickle
from sklearn.preprocessing import scale
import numpy as np

devName = '/dev/tty.c079-DevB'
folderName = 'data'

#2 sec

def setupBT():
	ser = serial.Serial(devName, 57600,timeout = 0.5)
	# ser.open()
	return ser

def HextoString(inputStr):
	value = int(inputStr, 16)
	if(value > 0x7FFF):
		value |= ~(0x1111)
	return value

def readBT(ser):
	stateReadData = False
	dataStore = {}
	dataStore['AcX'] = [0] * 500
	dataStore['AcY'] = [0] * 500
	dataStore['AcZ'] = [0] * 500
	dataStore['GyX'] = [0] * 500
	dataStore['GyY'] = [0] * 500
	dataStore['GyZ'] = [0] * 500
	dataStore['time'] = [0] * 500
	dataStore['FALL'] = [0] * 200
	ser.write('realtime')
	ser.flush()

	timecount = 0
	div = 10
	svm = get_svm()
	count = 0
	try:
		while True:
			if ser.inWaiting:
				inputStr = ser.readline()
				if stateReadData == False and 'ready' in inputStr:
					stateReadData = True
				elif stateReadData == True:
					if 'end' in inputStr:
						break

					dataHex = inputStr.replace(';', '').split(',')
					dataSet = map(HextoString, dataHex)
					try:
						dataStore['AcX'] = dataStore['AcX'][1:] + [float(dataSet[0])/8192]
						dataStore['AcY'] = dataStore['AcY'][1:] + [float(dataSet[1])/8192]
						dataStore['AcZ'] = dataStore['AcZ'][1:] + [float(dataSet[2])/8192]
						dataStore['GyX'] = dataStore['GyX'][1:] + [float(dataSet[3])/65.5]
						dataStore['GyY'] = dataStore['GyY'][1:] + [float(dataSet[4])/65.5]
						dataStore['GyZ'] = dataStore['GyZ'][1:] + [float(dataSet[5])/65.5]
						dataStore['FALL'] = dataStore['FALL'][1:] + [svm_predict(dataStore, svm)]
						fall, not_fall = dataStore['FALL'].count(1), dataStore['FALL'].count(0)
						print fall, not_fall
						if fall > not_fall:
							print 'THIS IS THE REAL FALLING !!!!!!'
					except KeyboardInterrupt as e:
						print 'Stoped'
						ser.write('stop')
						ser.flush()
						return
					except Exception as e:
						print e
	except KeyboardInterrupt as e:
		print 'Stoped'
		ser.write('stop')
		ser.flush()
		return


	ser.write('stop\n')
	ser.flush()		

	return dataStore

def get_svm():
	f = open('svm_real.model', 'rb')
	svm = pickle.load(f)
	f.close()
	return svm

def pad_zero(input_list):
	max_len = 1001
	if len(input_list) < max_len:
		return input_list + [0]*(max_len-len(input_list))

def svm_predict(dataStore, svm):
	X = dataStore['AcX'] + dataStore['AcY'] + dataStore['AcZ'] + dataStore['GyX'] + dataStore['GyY'] + dataStore['GyZ']
	# len(X)
	# print X
	X = scale(np.array([np.array(X)]), axis=1)
	# print X[0]
	# print X.shape
	# for x in X[0]:
	# 	print x
	result = svm.predict(X)
	if result:
		print 'FALL!!!'
	else:
		print 'Not FALL'

	return result
	

if __name__ == '__main__':
	ser = setupBT()
	while True:
		hh = raw_input('press enter if ready')
		print 'start'
		if hh == 'NO':
			break
		readBT(ser)
	ser.close()